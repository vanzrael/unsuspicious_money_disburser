<?php
define('DB_SERVER', "localhost");
define('DB_USERNAME', "root");
define('DB_PASSWORD', "");
define('DB_DATABASE', 'not_so_mini_flip_project');

function sql_connect(){
	$conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}
	
	return $conn;
}

function db_connect(){
	$conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	if(!$conn){
		die("Connection failed: " . mysqli_connect_error());
	}
	
	return $conn;
}
?>