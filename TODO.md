# To-do list

x. Create migration script (done) <br />
x. Create config files to access database (should suffice) <br />
x. Create pages <br />
x. Create forms <br />
x. Design classes <br />
x. Create API call <br />
x. Sanitize input (to avoid SQL Injection) <br />
x. Write proper README.md <br />