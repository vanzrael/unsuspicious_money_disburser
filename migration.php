<?php
require 'config.php';

$conn = sql_connect();

$create_db_comm = "CREATE DATABASE not_so_mini_flip_project";
$use_db_comm = "USE not_so_mini_flip_project";
$create_table_comm = "CREATE TABLE record (
	id INT PRIMARY KEY,
	amount INT(32) UNSIGNED NOT NULL,
	status VARCHAR(25),
	timestamp TIMESTAMP,
	bank_code VARCHAR(20) NOT NULL,
	account_number VARCHAR(15) NOT NULL,
	beneficiary_name VARCHAR(100),
	remark VARCHAR(100) NOT NULL,
	receipt TEXT,
	time_served TIMESTAMP,
	fee INT(32) UNSIGNED
)";

if(mysqli_query($conn, $create_db_comm)){
	echo "Create database successful\n";
} else {
	echo "Error creating database". mysqli_error($conn)."\n";
}

if(mysqli_query($conn, $use_db_comm)){
	echo "Go to selected database: not_so_mini_flip_project\n";
} else {
	echo "Error to go to selected database ".mysqli_error($conn)."\n";
}

if(mysqli_query($conn, $create_table_comm)){
	echo "Create table successful\n";
} else {
	echo "Error creating table ". mysqli_error($conn)."\n";
}
?>